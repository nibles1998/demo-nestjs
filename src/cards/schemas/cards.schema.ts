import * as mongoose from 'mongoose';

export const CardSchema = new mongoose.Schema({
    numberCard: String,
    type: String,
    expirationDate: Date
});