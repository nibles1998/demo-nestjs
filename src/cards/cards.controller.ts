import { Controller, Get, Post, Put, Delete, Body, Param, Query, Res, HttpStatus } from '@nestjs/common';
import { CreateCardDto } from './dto/create-card.dto';
import { CardsService } from './cards.service';
import { Card } from './interfaces/cards.interface';

@Controller('cards')
export class CardsController {
    constructor(private readonly cardsService: CardsService) { };

    @Post()
    async create(@Body() CreateCardDto: CreateCardDto, @Res() res) {
        await this.cardsService.create(CreateCardDto);
        return res.status(HttpStatus.OK).json({
            HttpStatus: HttpStatus.OK,
            message: "Card is created!"
        });
    };

    @Get('/s')
    async findAll(@Query() query, @Res() res): Promise<Card[]> {
        return res.status(HttpStatus.OK).json({
            HttpStatus: HttpStatus.OK,
            data: await this.cardsService.findAll(query)
        });
    };

    @Get(':_id')
    async findById(@Param('_id') id, @Res() res): Promise<Card> {
        return res.status(HttpStatus.OK).json({
            HttpStatus: HttpStatus.OK,
            data: await this.cardsService.findById(id)
        });
    }

    @Put(':_id')
    async update(@Param('_id') id, @Body() data, @Res() res) {
        await this.cardsService.update(id, data);
        return res.status(HttpStatus.OK).json({
            HttpStatus: HttpStatus.OK,
            message: "Update successfully!"
        });
    }

    @Delete(':_id')
    async delete(@Param('_id') id, @Res() res) {
        await this.cardsService.delete(id);
        return res.status(HttpStatus.OK).json({
            HttpStatus: HttpStatus.OK,
            message: "Card is deleted!"
        });
    }
};