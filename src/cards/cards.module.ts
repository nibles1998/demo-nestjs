import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { CardSchema } from './schemas/cards.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: "Card", schema: CardSchema }])],
    controllers: [CardsController],
    providers: [CardsService],
    exports: [
        CardsService,
        MongooseModule
    ]
})
export class CardsModule { };