export class CreateCardDto {
    readonly numberCard: String;
    readonly type: String;
    readonly expirationDate: Date;
};