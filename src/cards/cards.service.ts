import { Model } from 'mongoose';
import { ObjectID } from 'mongodb';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Card } from './interfaces/cards.interface';
import { CreateCardDto } from './dto/create-card.dto';

@Injectable()
export class CardsService {
    constructor(@InjectModel('Card') private readonly cardModel: Model<Card>) { };

    async findAll(query): Promise<Card[]> {
        const allKeys = Object.keys(query);
        const whereQuery = {};
        for (let index = 0; index < allKeys.length; index++) {
            const _queryKey = allKeys[index];
            if (_queryKey == "numbercard") {
                whereQuery["numberCard"] = query[_queryKey];
                continue;
            }
            if (_queryKey == "type") {
                whereQuery[_queryKey] = query[_queryKey];
                continue;
            }
        }
        return await this.cardModel.find(whereQuery);
    };

    async findById(id): Promise<Card> {
        if (!ObjectID.isValid(id))
            throw new BadRequestException("Invalid ID!")
        return await this.cardModel.findOne({ _id: id });
    }

    async create(CreateCardDto: CreateCardDto): Promise<Card> {
        const createdCard = new this.cardModel(CreateCardDto);
        return await createdCard.save();
    };

    async update(id, data): Promise<Card> {
        if (!ObjectID.isValid(id))
            throw new BadRequestException("Invalid ID!")
        return await this.cardModel.updateOne({ _id: id }, { $set: data }, { new: true });
    }

    async delete(id): Promise<Card> {
        if (!ObjectID.isValid(id))
            throw new BadRequestException("Invalid ID!")
        return await this.cardModel.deleteOne({ _id: id });
    }
}