import { Document } from 'mongoose';

export interface Card extends Document {
    readonly numberCard: String;
    readonly type: String;
    readonly expiration: Date;
};