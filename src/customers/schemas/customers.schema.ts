import * as mongoose from 'mongoose';

export const CustomerSchema = new mongoose.Schema({
    name: String,
    age: Number,
    numberCard: String,
    gender: Boolean
});