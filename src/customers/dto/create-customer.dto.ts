export class CreateCustomerDto {
    readonly name: String;
    readonly age: Number;
    readonly numberCard: String;
    readonly gender: Boolean;
};