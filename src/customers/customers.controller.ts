import { Controller, Get, Post, Body, Param, Put, Delete, Query, Res, HttpStatus } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { CustomersService } from './customers.service';
import { Customer } from './interfaces/customers.interface';

@Controller('customers')
export class CustomersController {
  constructor(private readonly customersService: CustomersService) { };

  @Post()
  async create(@Body() CreateCustomerDto: CreateCustomerDto, @Res() res) {
    await this.customersService.create(CreateCustomerDto);
    return res.status(HttpStatus.OK).json({
      HttpStatus: HttpStatus.OK,
      message: "Customer is created!"
    })
  }

  @Get('/s')
  async findAll(@Query() query, @Res() res): Promise<Customer[]> {
    return res.status(HttpStatus.OK).json({
      HttpStatus: HttpStatus.OK,
      data: await this.customersService.findAll(query)
    });
  }

  @Get(':_id')
  async findById(@Param('_id') id, @Res() res): Promise<Customer> {
    return res.status(HttpStatus.OK).json({
      HttpStatus: HttpStatus.OK,
      data: await this.customersService.findById(id)
    });
  }

  @Put(':_id')
  async update(@Param('_id') id, @Body() data, @Res() res) {
    await this.customersService.update(id, data);
    return res.status(HttpStatus.OK).json({
      HttpStatus: HttpStatus.OK,
      message: "Updade successfully!"
    });
  }

  @Delete(':_id')
  async delete(@Param('_id') id, @Res() res) {
    await this.customersService.delete(id);
    return res.status(HttpStatus.OK).json({
      HttpStatus: HttpStatus.OK,
      message: "Customer is deleted!"
    });
  }
}