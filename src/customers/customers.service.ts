import { Model } from 'mongoose';
import { ObjectID } from 'mongodb';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Customer } from './interfaces/customers.interface';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { Card } from '../cards/interfaces/cards.interface';
import { from } from 'rxjs';

@Injectable()
export class CustomersService {
  constructor(
    @InjectModel('Customer') private readonly customerModel: Model<Customer>,
    @InjectModel('Card') private readonly cardsModel: Model<Card>
  ) { };

  async create(CreateCustomerDto: CreateCustomerDto): Promise<Customer> {
    const createdCustomer = new this.customerModel(CreateCustomerDto);
    const cusNumberCard = CreateCustomerDto.numberCard;
    const whereQuery = { numberCard: cusNumberCard };
    const card = await this.cardsModel.find(whereQuery);
    if (Object.keys(card).length > 0)
      return await createdCustomer.save();
    else throw new BadRequestException("NumberCard don't exist!");
  }

  async findAll(query): Promise<Customer[]> {
    const allKeys = Object.keys(query);
    const whereQuery = {};
    for (let index = 0; index < allKeys.length; index++) {
      const _queryKey = allKeys[index];
      if (_queryKey == "name") {
        whereQuery[_queryKey] = query[_queryKey];
        continue;
      }
      if (_queryKey == "age") {
        whereQuery[_queryKey] = query[_queryKey] - 0;
        continue;
      }
      if (_queryKey == "numbercard") {
        whereQuery["numberCard"] = query[_queryKey];
        continue;
      }
      if (_queryKey == "gender") {
        if (query[_queryKey] == "nam" || query[_queryKey] == "male") {
          whereQuery[_queryKey] = true;
        } else if (query[_queryKey] == "nu" || query[_queryKey] == "female") {
          whereQuery[_queryKey] = false;
        } else throw new BadRequestException("Gender only accept 'nam|male' or 'nu|female'");
        continue;
      }
    }
    return await this.customerModel.find(whereQuery);
  }

  async findById(id): Promise<Customer> {
    if (!ObjectID.isValid(id))
      throw new BadRequestException("Invalid ID!")
    return await this.customerModel.findOne({ _id: id });
  }

  async update(id, data): Promise<Customer> {
    if (!ObjectID.isValid(id))
      throw new BadRequestException("Invalid ID!")
    const cusNumberCard = data.numberCard;
    if (cusNumberCard) {
      const whereQuery = { numberCard: cusNumberCard };
      const card = await this.cardsModel.find(whereQuery);
      if (Object.keys(card).length > 0)
        return await this.customerModel.updateOne({ _id: id }, { $set: data }, { new: true });
      else throw new BadRequestException("NumberCard don't exist!");
    }
    return await this.customerModel.updateOne({ _id: id }, { $set: data }, { new: true });
  }

  async delete(id): Promise<Customer> {
    if (!ObjectID.isValid(id))
      throw new BadRequestException("Invalid ID!")
    return await this.customerModel.deleteOne({ _id: id });
  }

}