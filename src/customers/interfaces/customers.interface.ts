export interface Customer {
    name: String;
    age: Number;
    numberCard: String;
    gender: Boolean;
}