import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CustomersModule } from './customers/customers.module';
import { CardsModule } from './cards/cards.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://mongoDBNibles:nibles1504@gettingstarted-5yzne.mongodb.net/bank?retryWrites=true&w=majority', { useFindAndModify: true }),
    CustomersModule,
    CardsModule
  ],
})
export class AppModule { };